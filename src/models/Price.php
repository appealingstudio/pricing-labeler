<?php

/**
 * Pricing Labeler
 *
 * Price Model
 * Data validation provided by Ardent package (https://github.com/laravelbook/ardent)
 * 
 * @copyright Appealing Studio Inc.
 * @author  Daniel Márquez Martínez <daniel.martinez@appealingstudio.com>
 */

namespace AppealingStudio\PricingLabeler;

use LaravelBook\Ardent\Ardent;

class Price extends Ardent
{
	/**
	 * Ardent options
	 */
    public $autoHydrateEntityFromInput = true;    // hydrates on new entries' validation
	public $forceEntityHydrationFromInput = true; // hydrates whenever validation is called

    // --------------------------------------------------------------------

	/**
	 * Attributes that should be mutated to dates
	 */
	protected $dates = array('created_at', 'updated_at');

    // --------------------------------------------------------------------

	/**
	 * Mass assignable attributes
	 */
	protected $fillable = array(
		'price',
		'price_label',
	);

    // --------------------------------------------------------------------

	/**
	 * Validation rules
	 */
	public static $rules = array(
		'price' => 'required|numeric',
		'price_label' => 'required',
	);

}