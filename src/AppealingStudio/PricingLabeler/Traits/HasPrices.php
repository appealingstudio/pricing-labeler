<?php

/**
 * Pricing Labeler
 *
 * HasPrices Trait
 * 
 * @copyright Appealing Studio Inc.
 * @author  Daniel Márquez Martínez <daniel.martinez@appealingstudio.com>
 */

namespace AppealingStudio\PricingLabeler\Traits;

use Illuminate\Support\MessageBag;
use AppealingStudio\PricingLabeler\Price;

Trait HasPrices
{
    /**
     * Protected variables
     */
    protected $_savePrices = TRUE;
    protected $_singlePricePrefix = NULL;
    protected $_singlePriceInput = array();
    protected $_multiplePricesPrefix = NULL;
    protected $_multiplePricesInput = array();
    protected $_removePricesNonUpdated = TRUE;
    protected $_addPriceErrorsToParentModel = TRUE;

    // --------------------------------------------------------------------

    /**
     * Relationship
     * 
     * @return Price array
     */
    public function prices()
    {
        return $this->morphMany('AppealingStudio\PricingLabeler\Price', 'priceable')->orderBy('id', 'asc');
    }

    // --------------------------------------------------------------------

    /**
     * Get lower price for this parent model
     * 
     * @return mixed (Price or NULL)
     */
    public function getLowestPriceAttribute()
    {
        $prices = $this->prices->sortBy(function($price)
        {
            return $price->price;
        });

        return ($prices) ? $prices->first()->price : NULL;
    }

    // --------------------------------------------------------------------

    /**
     * Get parent model prices array in the form
     * [id => [price => <price>, $price_labe => <price_label>], ...]
     * 
     * @return array
     */
    public function getPricesArray()
    {
        $result = array();

        foreach ($this->prices as $price)
        {
            $result[$price->id] = array(
                'price' => $price->price,
                'price_label' => $price->price_label,
            );
        }

        return $result;
    }

    // --------------------------------------------------------------------

    /**
     * Check if parentmodel has passed price id
     * 
     * @param  integer  $priceId
     * @return boolean
     */
    public function hasPriceID($priceId)
    {
        foreach ($this->prices as $price)
        {
            if ($price->id == $priceId)
            {
                return TRUE;
            }
        }

        return FALSE;
    }

    // --------------------------------------------------------------------

    /**
     * Get price by id
     * 
     * @param  integer $priceId
     * @return mixed (price or NULL)
     */
    public function getPriceById($priceId)
    {
        if ($this->hasPriceID($priceId))
        {
            $price = $this->prices->filter(function($price) use($priceId)
            {
                return ($price->id == $priceId);
            });

            return $price->first()->price;
        }

        return NULL;
    }

    // --------------------------------------------------------------------

    /**
     * Set single price input
     * 
     * @param array $singlePriceInput
     * @return this
     */
    public function setSinglePriceInput($singlePriceInput = NULL)
    {
        $this->_singlePriceInput = ($singlePriceInput) ?: $singlePriceInput;

        return $this;
    }

    // --------------------------------------------------------------------

    /**
     * Set multiple prices input
     * 
     * @param array $multiplePricesInput
     * @return this
     */
    public function setMultiplePricesInput($multiplePricesInput = NULL)
    {
        $this->_multiplePricesInput = ($multiplePricesInput) ?: $multiplePricesInput;

        return $this;
    }

    // --------------------------------------------------------------------

    /**
     * Set single price prefix
     * 
     * @param string $singlePricePrefix
     */
    public function setSinglePricePrefix($singlePricePrefix = NULL)
    {
        $this->_singlePricePrefix = ($singlePricePrefix) ?: $singlePricePrefix;

        return $this;
    }

    // --------------------------------------------------------------------

    /**
     * Set multiple prices prefix
     * 
     * @param string $multiplePricesPrefix
     */
    public function setMultiplePricesPrefix($multiplePricesPrefix = NULL)
    {
        $this->_multiplePricesPrefix = ($multiplePricesPrefix) ?: $multiplePricesPrefix;

        return $this;
    }

    // --------------------------------------------------------------------

    /**
     * Set save prices option
     * 
     * @param boolean $savePrices
     */
    public function setSavePrices($savePrices = TRUE)
    {
        $this->_savePrices = ($savePrices) ?: $savePrices;

        return $this;
    }

    // --------------------------------------------------------------------

    /**
     * Set remove prices non updated option
     * 
     * @param boolean $removePricesNonUpdated [description]
     */
    public function setRemovePricesNonUpdated($removePricesNonUpdated = TRUE)
    {
        $this->_removePricesNonUpdated = ($removePricesNonUpdated) ?: $removePricesNonUpdated;

        return $this;
    }

    // --------------------------------------------------------------------

    /**
     * Set add price errors to parent model
     * 
     * @param boolean $addPriceErrorsToParentModel
     */
    public function setAddPriceErrorsToParentModel($addPriceErrorsToParentModel = TRUE)
    {
        $this->_addPriceErrorsToParentModel = ($addPriceErrorsToParentModel) ?: $addPriceErrorsToParentModel;

        return $this;
    }

    // --------------------------------------------------------------------

    /**
     * Store multiple prices
     * 
     * @return Price Model array
     */
    public function storeMultiplePrices()
    {
        $result = array();
        $price_ids = array();
        
        // Create or update prices
        foreach ($this->_multiplePricesInput as $key => $priceInput)
        {
            $this->_singlePriceInput = $priceInput;
            $this->_singlePricePrefix = $this->_multiplePricesPrefix . '.' . $key;

            $result[$key] = $this->storeSinglePrice();

            if ($this->_removePricesNonUpdated)
            {
                $price_ids[] = $result[$key]->id;
            }
        }
        
        // Remove non updated prices if required
        if ($this->_removePricesNonUpdated)
        {
            foreach ($this->prices as $key =>$price)
            {
                if (!in_array($price->id, $price_ids))
                {
                    if ($this->_savePrices)
                    {
                        $price->delete();
                    }
                    else
                    {
                        $this->prices->forget($key);
                    }
                }
            }
        }

        return $result;
    }

    // --------------------------------------------------------------------

    /**
     * Store single price
     * 
     * @return Price model
     */
    public function storeSinglePrice()
    {
        $id = array_get($this->_singlePriceInput, 'id');

        $price = (is_numeric($id) && !empty($id))
            ? Price::find($id)
            : new Price;

        $price->fill($this->_singlePriceInput);

        if ($this->_savePrices)
        {
            $price->save();
            $price = $this->adjustErrorKeys($price);
            $this->prices()->save($price);
        }
        else
        {
            $price->validate();
            $price = $this->adjustErrorKeys($price);
            $this->prices->add($price);
        }

        return $price;
    }

    // --------------------------------------------------------------------

    /**
     * Adjust error keys
     *
     * 1.- Creates a new MessageBag instance
     * 2.- Iterates over the prices errors
     * 3.- Clones errors into the new MessageBag instance adjusting the key to fit input names
     * 4.- Insert that new error keys on the existing MessageBag
     * 5.- Optionally, errors can be also injected directly into parent model
     * 
     * The resulting MessageBag can be injected directy on the view as 'withErrors()'' method to highlight fields
     *
     * TODO: Probably this method needs to be in some kind of Helper Package
     * 
     * @param  Price $price  
     * @return Price
     */
    public function adjustErrorKeys($price)
    {
        if ($this->_singlePricePrefix == NULL)
        {
            return $price;
        }

        $adjustedMessages = new MessageBag;
        foreach ($price->errors()->getMessages() as $field => $messages)
        {
            foreach ($messages as $message)
            {
                $adjustedMessages->add($this->_singlePricePrefix . '.' . $field, $message);

                if ($this->_addPriceErrorsToParentModel)
                {
                    $this->errors()->add($this->_singlePricePrefix . '.' . $field, $message);
                }
            }
        }

        return $price;
    }
}