<?php

/**
 * Pricing Labeler
 *
 * Pricing Labelet Service Provider
 * 
 * @copyright Appealing Studio Inc.
 * @author  Daniel Márquea Martínez <daniel.martinez@appealingstudio.com>
 */

namespace AppealingStudio\PricingLabeler;

use Illuminate\Support\ServiceProvider;

class PricingLabelerServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('appealing-studio/pricing-labeler');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}